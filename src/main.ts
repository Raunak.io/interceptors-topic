import { LoggingInterceptor } from './logging.interceptor';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalInterceptors(new LoggingInterceptor()); // used across the application
  await app.listen(3000);
}
bootstrap();
