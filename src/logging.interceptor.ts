import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  BadGatewayException,
  RequestTimeoutException,
} from '@nestjs/common';

import { Observable, throwError, TimeoutError } from 'rxjs';
import { tap, catchError, timeout } from 'rxjs/operators';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  intercept(contect: ExecutionContext, next: CallHandler): Observable<any> {
    console.log(' before....');
    const now = Date.now();

    return (
      next
        .handle()
        //   .pipe(tap(() => console.log(`after ${Date.now() - now} mss`)));
        .pipe(
          timeout(5000), // means 5 sec
          catchError(err => {
            if (err instanceof TimeoutError) {
              return throwError(new RequestTimeoutException());
            }

            return throwError(err);
          }),
        )
    );
  }
}
